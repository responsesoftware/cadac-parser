package nl.response.cadacparser;

public class FunctionTableRow {

    private String function = "";
    private String description = "";
    private String workCenter = "";
    private String costCenter = "";
    private String standardOperation = "";
    private String type = "";
    private String machineFactor = "";
    private String laborFactor = "";

    public FunctionTableRow() {
    }

    public FunctionTableRow(String function, String description, String workCenter, String costCenter, String standardOperation, String type, String machineFactor, String laborFactor) {
        this.function = function;
        this.description = description;
        this.workCenter = workCenter;
        this.costCenter = costCenter;
        this.standardOperation = standardOperation;
        this.type = type;
        this.machineFactor = machineFactor;
        this.laborFactor = laborFactor;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWorkCenter() {
        return workCenter;
    }

    public void setWorkCenter(String workCenter) {
        this.workCenter = workCenter;
    }

    public String getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    public String getStandardOperation() {
        return standardOperation;
    }

    public void setStandardOperation(String standardOperation) {
        this.standardOperation = standardOperation;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMachineFactor() {
        return machineFactor;
    }

    public void setMachineFactor(String machineFactor) {
        this.machineFactor = machineFactor;
    }

    public String getLaborFactor() {
        return laborFactor;
    }

    public void setLaborFactor(String laborFactor) {
        this.laborFactor = laborFactor;
    }

    @Override
    public String toString() {
        return "FunctionTableRow{" +
                "function='" + function + '\'' +
                ", description='" + description + '\'' +
                ", workCenter='" + workCenter + '\'' +
                ", costCenter='" + costCenter + '\'' +
                ", standardOperation='" + standardOperation + '\'' +
                ", type='" + type + '\'' +
                ", machineFactor='" + machineFactor + '\'' +
                ", laborFactor='" + laborFactor + '\'' +
                '}';
    }
}
