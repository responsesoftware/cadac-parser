package nl.response.cadacparser.helpers;

import java.io.File;

public class FileObject {
    private File fileObject;
    private boolean movedToErrorLocation = false;

    public FileObject(File aFile) {
        fileObject = aFile;
    }

    public void setMovedToErrorLocation() {
        movedToErrorLocation = true;
    }

    public boolean isMovedToErrorLocation() {
        return movedToErrorLocation;
    }

    public String getName() {
        return fileObject.getName();
    }

    public File getFile() {
        return fileObject;
    }

}
