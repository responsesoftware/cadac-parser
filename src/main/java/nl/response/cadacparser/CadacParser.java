package nl.response.cadacparser;

import nl.response.cadacparser.dataObjecten.*;
import nl.response.cadacparser.exceptions.WrongDateFormatException;
import nl.response.cadacparser.exceptions.WrongMeasurementFormatException;
import nl.response.cadacparser.helpers.FileObject;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.*;
import java.math.RoundingMode;
import java.nio.file.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

public class CadacParser {

  // Definition of required properties benodigde property namen
  private final String SOURCE_FILE_OR_DIRECTORY = "source";
  private final String DESTINATION_DIRECTORY = "destination";
  private final String FUNCTION_TABLE_LOCATION = "functiontablelocation";
  private final String PROCESSED_DIRECTORY = "processed";
  private final String ERROR_DIRECTORY = "error";
  private final String LAST_SEQUENCE = "lastsequence";
  private final String SOURCE_TRADING_PARTNER = "sourcetradingpartner";
  private final String ITEM_CATALOG_INCLUDED_FLAG = "itemcatalogincludedflag";
  private final String ITEM_PLANNING_INCLUDED_FLAG = "itemplanningincludedflag";
  private final String BOM_INCLUDED_FLAG = "bomincludedflag";
  private final String ROUTING_INCLUDED_FLAG = "routingincludedflag";
  private final String WEIGHT_UNIT_OF_MEASURE = "weightunitofmeasure";
  private final String BUYING_UNIT_OF_MEASURE = "buyingunitofmeasure"; // Is available in properties but will be calculated from source
  private final String DIRECT_COST_FLAG = "directcostflag";
  private final String LOT_CONTROLLED_FLAG = "lotcontrolledflag";
  private final String QA_STATUS_CODE = "qastatuscode";
  private final String SELL_UNIT_OF_MEASURE = "sellunitofmeasure"; // Is available in properties but will be calculated from source
  private final String STOCK_UNIT_OF_MEASURE = "stockunitofmeasure"; // Is available in properties but will be calculated from source
  private final String STOCK_ITEM = "stockitem";
  private final String VAT_CODE = "vatcode";
  private final String PRP_ITEM_FLAG = "prpitemflag";
  private final String BOM_COST_ROUTING_TYPE_CODE = "bomcostroutingtypecode";
  private final String DESTINATION_TRADING_PARTNER = "destinationtradingpartner";
  private final String CORPORATION_CONTROL_NUMBER = "corporationcontrolnumber";
  private final String ACCOUNTING_GROUP_ID = "accountinggroupid";
  private final String STANDARD_COST = "standardcost";
  private final String COST_METHOD = "costmethod";
  private final String BCR_TYPE_CODE = "bcrtypecode";
  private final String SOURCE_REFERENCE = "sourcereference";
  private final String DESTINATION_REFERENCE = "destinationreference";
  private final String BILL_OF_MATERIAL_INCREMENT = "billofmaterialincrement";
  private final String ROUTING_INCREMENT = "routingincrement";
  private final String SOURCE_ITEM_FLAG = "sourceitemflag";
  private final String ASPCPO_MASTER_LOCATION = "aspcpomasterlocation";
  private final String STANDARD_UM = "standardum";
  private final String ROUTING_UM = "routingum";
  private final String DEBUG = "debug";

  // other variables
  private static Properties allProperties = null;
  private static Map<String,FunctionTableRow> functionMap = new HashMap<>();
  private static Map<String,PartRow> existingPartsMap = new HashMap<>();
  private final String INSTALL_DIRECTORY = ".";
  private static final String VERSION = "3.1.6";
  private static final Logger log = Logger.getLogger(String.valueOf(CadacParser.class));
  private PackageData pd = new PackageData();
  private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
  private boolean isDebugOn = false;

  /**
   * This method will check the files/directories in the property file to see if they contain what
   * we expect.
   *
   * @throws ParserConfigurationException
   * @throws IOException
   * @throws SAXException
   */
  private void letsStart() {
    allProperties = readProperties();
    readFunctionTableData();
    readExistingPartsTable();
    if(allProperties.getProperty(DEBUG).equalsIgnoreCase("true")) {
      isDebugOn = true;
    }

    // Checking the DESTINATION DIRECTORY
    File outputDirectory = new File(allProperties.getProperty(DESTINATION_DIRECTORY));
    checkOutputDirectory(outputDirectory);

    File processedDirectory = new File(allProperties.getProperty(PROCESSED_DIRECTORY));
    checkOutputDirectory(processedDirectory);

    File errorDirectory = new File(allProperties.getProperty(ERROR_DIRECTORY));
    checkOutputDirectory(errorDirectory);

    // Now check the input thingy. Is it a file or directory
    File xmlFileName = new File(allProperties.getProperty(SOURCE_FILE_OR_DIRECTORY));
    if (!xmlFileName.exists()) {
      // What if the given source file or directory does not exist?
      log.info("The given file '" + allProperties.getProperty(SOURCE_FILE_OR_DIRECTORY) + "'");
      log.info("does not exist. Did you enter the correct path?");
      finishErrorMelding();
    } else if (xmlFileName.isFile()) {
      FileObject fo = new FileObject(xmlFileName);
      try {
        // parse the file
        parseTheFile(fo);
        // After parsing we can create the output file
        createNewXML(fo.getName());
      } catch (IOException | SAXException | ParserConfigurationException ioe) {
        moveFileToErrorLocation(fo);
      }
      if (!fo.isMovedToErrorLocation()) {
        moveTheFile(fo.getFile());
      }
    } else if (xmlFileName.isDirectory()) {
      // So it is a directory. Handle all files that match. (ending in .xml or .XML  and NOT ending
      // in -Glovia.xml or -Glovia.XML)
      File[] allFiles =
          xmlFileName.listFiles(
              pathname -> {
                return (pathname.getName().endsWith(".xml") || pathname.getName().endsWith(".XML"))
                        && !(pathname.getName().endsWith("-Glovia.xml")
                        || pathname.getName().endsWith("-Glovia.XML"));
              });
      if (allFiles != null) {
        Arrays.sort(allFiles, Comparator.comparingLong(File::lastModified));
        for (File oneFile : allFiles) {
          FileObject fo = new FileObject(oneFile);
          // clean internal representation otherwise we get all mixed up information
          pd = new PackageData();
          try {
            parseTheFile(fo);
            // After parsing we can create the output file
            createNewXML(fo.getName());
          } catch (IOException | SAXException | ParserConfigurationException ioe) {
            moveFileToErrorLocation(fo);
          }
          if (!fo.isMovedToErrorLocation()) {
            moveTheFile(fo.getFile());
          }
        }
      }
    }
  }

  private void moveFileToErrorLocation(FileObject fileObject) {
    if(isDebugOn) {
      log.info("Bestand bevat onbekende data en is naar error locatie verplaatst: " + fileObject.getName());
    }
    File theFile = fileObject.getFile();
    try {
      Path temp =
          Files.move(
              Paths.get(theFile.getAbsolutePath()),
              Paths.get(
                  allProperties.getProperty(ERROR_DIRECTORY) + File.separator + theFile.getName()));
      if (temp == null) {
        log.info("Unable to move file to " + allProperties.getProperty(ERROR_DIRECTORY));
      }
      fileObject.setMovedToErrorLocation();
    } catch (IOException ioe) {
      log.warning(
          "Error while moving " + theFile + " to " + allProperties.getProperty(ERROR_DIRECTORY));
      ioe.printStackTrace();
    }
  }

  private void moveTheFile(File name) {
    if(isDebugOn) {
      log.info("Bestand is correct behandeld en verplaatst: " + name.getName());
    }
    try {
      File checkFile = new File(allProperties.getProperty(PROCESSED_DIRECTORY) + File.separator + name.getName());
      if(checkFile.exists()) {
        log.warning("The file with name: " + checkFile.getAbsolutePath() + " already exists in the " + allProperties.getProperty(DESTINATION_DIRECTORY) + " directory.");
        return;
      }
      Path temp =
          Files.move(
              Paths.get(name.getAbsolutePath()),
              Paths.get(
                  allProperties.getProperty(PROCESSED_DIRECTORY)
                      + File.separator
                      + name.getName()));
      if (temp == null) {
        log.info("Unable to move" + name + " to " + allProperties.getProperty(PROCESSED_DIRECTORY));
      }
    } catch (IOException ioe) {
      log.warning(
          "Error while moving " + name + " to " + allProperties.getProperty(PROCESSED_DIRECTORY));
      ioe.printStackTrace();
    }
  }

  /**
   * Method to start the parsing of the document
   *
   * @param xmlFileName The complete path and filename of the file to parse.
   */
  private void parseTheFile(FileObject xmlFileName)
      throws IOException, SAXException, ParserConfigurationException {
    // Let's start with getting the file and prepare the parser
    if(isDebugOn) {
      log.info("Start behandeling van bestand: " + xmlFileName.getName());
    }
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setNamespaceAware(true); // never forget this!
    DocumentBuilder builder = factory.newDocumentBuilder();
    Document doc = builder.parse(xmlFileName.getFile());

    XPathFactory xpathfactory = XPathFactory.newInstance();
    XPath xpath = xpathfactory.newXPath();

    try {
      if(isDebugOn) {
        log.info("Starting the parsing of PackageData");
      }
      XPathExpression expr = xpath.compile("//PackageData");
      Object result = expr.evaluate(doc, XPathConstants.NODE);
      Node packageDataNode = (Node) result;

      pd.setDescription(packageDataNode.getAttributes().getNamedItem("description").getNodeValue());
      pd.setDocumentCreateDate(
          packageDataNode.getAttributes().getNamedItem("documentCreateDate").getNodeValue());
      pd.setDocumentID(packageDataNode.getAttributes().getNamedItem("documentID").getNodeValue());
      pd.setDocumentModificationDate(
          packageDataNode.getAttributes().getNamedItem("documentModificationDate").getNodeValue());
      pd.setSetOriginatedByID(
          packageDataNode.getAttributes().getNamedItem("originatedByID").getNodeValue());
      pd.setOriginatorByName(
          packageDataNode.getAttributes().getNamedItem("originatorByName").getNodeValue());

      // Getting the Items node from PackageData (Only one)
      result = expr.evaluate(doc, XPathConstants.NODESET);
      NodeList packageNode = (NodeList) result;
      int packageDataNodeLength = packageNode.getLength();
      if(isDebugOn) {
        log.info("Going in to PackageData loop. Found " + packageDataNodeLength + " node(s). (Should be 1)");
      }
      for (int pDIndex = 0; pDIndex < packageDataNodeLength; pDIndex++) {
        Node pDNode = packageNode.item(pDIndex);

        // Getting the only child Node that should be of type Items
        Node itemsNode = pDNode.getFirstChild();
        while (itemsNode == null
            || itemsNode.getLocalName() == null
            || !itemsNode.getLocalName().equals("Items")) {
          itemsNode = itemsNode.getNextSibling();
        }
        // Getting all the Item nodes.
        NodeList itemNodes = itemsNode.getChildNodes();
        int numberOfItemNodes = itemNodes.getLength();
        if(isDebugOn) {
          log.info("Number of child Item nodes: " + numberOfItemNodes);
          log.info("Going over all Item nodes");
        }
        for (int itemNodeIndex = 0; itemNodeIndex < numberOfItemNodes; itemNodeIndex++) {
          Node itemNode = itemNodes.item(itemNodeIndex);
          if (itemNode.getLocalName() == null || !itemNode.getLocalName().equals("Item")) {
            continue;
          }
          Item anItem = new Item();
          anItem.setItemID(itemNode.getAttributes().getNamedItem("itemID").getNodeValue());
          anItem.setRevisionID(itemNode.getAttributes().getNamedItem("revisionID").getNodeValue());
          anItem.setIsTopLevel(itemNode.getAttributes().getNamedItem("isTopLevel").getNodeValue());
          checkValidityOfPart(anItem);
          if(anItem.isExistingPart()) {
            continue;
          }
          anItem.setCategory(itemNode.getAttributes().getNamedItem("category").getNodeValue());
          anItem.setDescription(
              itemNode.getAttributes().getNamedItem("description").getNodeValue());
          anItem.setEffectivity(
              itemNode.getAttributes().getNamedItem("effectivity").getNodeValue());
          anItem.setEffectivityEnd(
              itemNode.getAttributes().getNamedItem("effectivityEnd").getNodeValue());
          anItem.setEffectivityStart(
              itemNode.getAttributes().getNamedItem("effectivityStart").getNodeValue());
          anItem.setGlobalAccuracy(
              itemNode.getAttributes().getNamedItem("globalAccuracy").getNodeValue());
          anItem.setGlobalLifeCyclePhase(
              itemNode.getAttributes().getNamedItem("globalLifeCyclePhase").getNodeValue());
          anItem.setGlobalLifeCyclePhaseOtherDescription(
              itemNode
                  .getAttributes()
                  .getNamedItem("globalLifeCyclePhaseOtherDescription")
                  .getNodeValue());
          anItem.setGlobalMinQuantity(
              Byte.parseByte(
                  itemNode.getAttributes().getNamedItem("globalMinQuantity").getNodeValue()));
          anItem.setGlobalProductType(
              itemNode.getAttributes().getNamedItem("globalProductType").getNodeValue());
          anItem.setGlobalUnitOfMeasure(
              itemNode.getAttributes().getNamedItem("globalUnitOfMeasure").getNodeValue());
          anItem.setIsTopLevel(itemNode.getAttributes().getNamedItem("isTopLevel").getNodeValue());
          anItem.setItemClassification(
              itemNode.getAttributes().getNamedItem("itemClassification").getNodeValue());
          anItem.setItemUniqueID(
              itemNode.getAttributes().getNamedItem("itemUniqueID").getNodeValue());
          anItem.setLifeCycleDefnName(
              itemNode.getAttributes().getNamedItem("lifeCycleDefnName").getNodeValue());
          anItem.setMakeBuy(itemNode.getAttributes().getNamedItem("makeBuy").getNodeValue());
          anItem.setMakeBuyOtherDescription(
              itemNode.getAttributes().getNamedItem("makeBuyOtherDescription").getNodeValue());
          anItem.setOwnerContactID(
              Byte.parseByte(
                  itemNode.getAttributes().getNamedItem("ownerContactID").getNodeValue()));
          anItem.setOwnerName(itemNode.getAttributes().getNamedItem("ownerName").getNodeValue());
          anItem.setRevisionReleasedDate(
              createDate(
                  itemNode.getAttributes().getNamedItem("revisionReleasedDate").getNodeValue()));
          anItem.setTitle(itemNode.getAttributes().getNamedItem("title").getNodeValue());



          // Getting all the other child nodes.
          NodeList childNodes = itemNode.getChildNodes();
          int numberOfChildNodes = childNodes.getLength();
          for (int index = 0; index < numberOfChildNodes; index++) {
            Node someNode = childNodes.item(index);
            String nodeName = someNode.getNodeName();
            switch (nodeName) {
              case "OtherAttributes":
                handleOtherAttributes(someNode, anItem);
                if (anItem.getOtherAttributes() != null) {
                  OtherAttributes otherAttributes = anItem.getOtherAttributes();
                  List<Attribute> listOfAttributes = otherAttributes.getAttributes();
                  for (Attribute anAttribute : listOfAttributes) {
                    if (anAttribute.getName().equals("RawMaterial")
                        && anAttribute.getValue() != null
                        && anAttribute.getValue().length() > 0) {
//                      if(isItAnExistingPart(anAttribute.getValue(), " ")) {
//                        continue;
//                      }
                      try {
                        createSpecialBillOfMaterial(someNode, anItem);
                      } catch (WrongMeasurementFormatException wmfe) {
                        moveFileToErrorLocation(xmlFileName);
                      }
                    }
                  }
                }
                break;
              case "BillOfMaterial":
                handleBillOfMaterial(someNode, anItem);
                break;
              case "Attachments":
                handleAttachments(someNode, anItem);
                break;
              default:
                log.warning("Unknown element found while parsing ItemNode: " + nodeName);
            }
          }
          // Add the found item to the PackageData object
          pd.addItem(anItem);

          if(isDebugOn) {
            log.info("Done with reading the file: " + xmlFileName.getName());
            log.info("Now going to create the output");
          }
        }
      }

    } catch (XPathExpressionException e) {
      log.severe("Parsing problems: " + e.getMessage());
    } catch (WrongDateFormatException wdfe) {
      log.severe(wdfe.getMessage());
    }
  }

  private void checkValidityOfPart(Item anItem) {
    // If an item is not a TopLevel item then skip it.
    if(anItem.getIsTopLevel().equalsIgnoreCase("No")) {
      // Can easily be done by making it an existing part.
      // Existing parts will be filtered out later on.
      if(isDebugOn) {
        log.info("Dit is geen toplevel item dus slaan we over: " + anItem.getItemID());
      }
      anItem.setExistingPart(true);
      return;
    }
    // Check item with parts in existingParts map.
    // If there is a match then do not continue with this item.
    if(isItAnExistingPart(anItem.getItemID(), anItem.getRevisionID())) {
      anItem.setExistingPart(true);
    }
  }

  /**
   * Helper method to create a BillOfMaterial if OtherAttributes has a field "RawMaterial" with a
   * filled "value" field.
   *
   * @param someNode The node that can contain the RawMaterial field
   * @param itemNode The node contains all information.
   */
  private void createSpecialBillOfMaterial(Node someNode, Item itemNode)
      throws WrongMeasurementFormatException {
    // Using deprecated constructor for compatibility with Java <19
    Locale.setDefault(new Locale("nl", "NL"));
    NumberFormat nf = NumberFormat.getNumberInstance(Locale.getDefault());
    DecimalFormat df = (DecimalFormat)nf;
    df.applyPattern("###.####");

    OtherAttributes otherAttributes = itemNode.getOtherAttributes();
    List<Attribute> attributes = otherAttributes.getAttributes();
    BillOfMaterial bom = new BillOfMaterial();
    String lengte = "";
    String breedte = "";
    String hoogte = "";
    String massa = "";
    String itemId = "";
    String type = "";
    for (Attribute anAttribute : attributes) {
      switch (anAttribute.getName()) {
        case "RawMaterial":
          if (anAttribute.getValue() == null || anAttribute.getValue().length() == 0) {
            return;
          }
          itemId = anAttribute.getValue();
          continue;
        case "RawMaterialLength":
          lengte = getParsableValue(anAttribute.getValue());
          continue;
        case "RawMaterialUnit":
          type = anAttribute.getValue();
          continue;
        case "RawMaterialWidth":
          breedte = getParsableValue(anAttribute.getValue());
          continue;
        case "RawMaterialHeight":
          hoogte = getParsableValue(anAttribute.getValue());
          continue;
        case "TotalMass":
          massa = getParsableValue(anAttribute.getValue());
          continue;
        default:
      }
    }
    BillOfMaterialItem aBillOfMaterialItem = new BillOfMaterialItem();
    aBillOfMaterialItem.setBillOfMaterialItemID(itemId);
    // aBillOfMaterialItem.setBillOfMaterialItemUniqueID(attachmentNode.getAttributes().getNamedItem("billOfMaterialItemUniqueID").getNodeValue());
    // aBillOfMaterialItem.setBillOfMaterialType(attachmentNode.getAttributes().getNamedItem("billOfMaterialType").getNodeValue());
    // aBillOfMaterialItem.setBillOfMaterialTypeOtherDescription(attachmentNode.getAttributes().getNamedItem("billOfMaterialTypeOtherDescription").getNodeValue());
    // aBillOfMaterialItem.setBillOfMaterialUnitOfMeasure(attachmentNode.getAttributes().getNamedItem("billOfMaterialUnitOfMeasure").getNodeValue());
    // aBillOfMaterialItem.setDescription(attachmentNode.getAttributes().getNamedItem("description").getNodeValue());
    // aBillOfMaterialItem.setDetailID(attachmentNode.getAttributes().getNamedItem("detailID").getNodeValue());
    // aBillOfMaterialItem.setInstanceCount(Byte.parseByte(attachmentNode.getAttributes().getNamedItem("instanceCount").getNodeValue()));
    if (type == null || type.length() == 0 || type.equals("Each")) {
      // Do Nothing
    } else if (type.equals("Length")) {
      Double lengteValue = parseRawMaterialValues(lengte);
      aBillOfMaterialItem.setItemQuantity(df.format(lengteValue));
    } else if (type.equals("Area")) {
      Double lengteValue = parseRawMaterialValues(lengte);
      Double breedteValue = parseRawMaterialValues(breedte);
      aBillOfMaterialItem.setItemQuantity(
          df.format(makeItSquaredMeters(lengteValue * breedteValue)));
    } else if (type.equals("Volume")) {
      Double lengteValue = parseRawMaterialValues(lengte);
      Double breedteValue = parseRawMaterialValues(breedte);
      Double hoogteValue = parseRawMaterialValues(hoogte);
      double volume =
              lengteValue * breedteValue * hoogteValue;
      aBillOfMaterialItem.setItemQuantity(df.format(makeItQubicMeters(volume)));
    }
    // aBillOfMaterialItem.setNotes(attachmentNode.getAttributes().getNamedItem("notes").getNodeValue());
    aBillOfMaterialItem.setPositionNumber((byte) 1);
    aBillOfMaterialItem.setRevisionID("-");
    aBillOfMaterialItem.setRowOrder((byte) 1);
    // aBillOfMaterialItem.setUnitSize(attachmentNode.getAttributes().getNamedItem("unitSize").getNodeValue());
    bom.addBillOfMaterialItem(aBillOfMaterialItem);
    itemNode.addBillOfMaterial(bom);
  }

  private String getParsableValue(String aStringValue) {
    if (aStringValue.matches("^\\d+,?\\d* ?\\w*$")) {
      return aStringValue.replace(",", ".");
    }
    return aStringValue;
  }
  /**
   * Helper method to parse a String value getting rid of the unit value
   *
   * @param someValue A String to parse. It should contain a value and possibly a kind of unit
   *     information like mm or m
   * @return the value in mm.
   */
  private double parseRawMaterialValues(String someValue) throws WrongMeasurementFormatException {
    // split the string in a value and a unit.
    // If no unit part then quit processing and place file in error dir.
    // Make mm of the value (If needed).
    double value = 0.0;
    String tempString = someValue.trim();
    if (tempString.matches("^\\d+\\.?\\d+ [a-z-A-Z]{1,2}$")) {
      StringTokenizer st = new StringTokenizer(tempString, " ");
      String valueAsString = st.nextToken();
      String unitValue = st.nextToken();
      value = Double.parseDouble(valueAsString);
      switch (unitValue) {
        case "m":
        case "M":
          value *= 1000;
          break;
        case "dm":
        case "DM":
        case "Dm":
          value *= 100;
          break;
        case "cm":
        case "CM":
        case "Cm":
          value *= 10;
          break;
        default:
          // The value is in mm which is the default. So nothing to do here.
          break;
      }
    } else {
      log.warning(
          "The given measurement information is not complete or wrong: "
              + tempString
              + ". The file will be moved to error location.");
      throw new WrongMeasurementFormatException(
          "The given measurement information is not complete or wrong: "
              + tempString
              + ". The file will be moved to error location.");
    }
    return value;
  }

  /**
   * Helper method to make a mm2 value into a m2 value. As all values are in mm we need to divide
   * the value by 1 Million.
   *
   * @param areaInMM The value to transform
   * @return the transformed value in m2
   */
  private double makeItSquaredMeters(double areaInMM) {
    return areaInMM / 1000000;
  }

  /**
   * Helper method to make a mm3 value into a m3 value. As all values are in mm we need to divide
   * the value by 1000 Million.
   *
   * @param volumeInMM The value to transform
   * @return the transformed value in m3
   */
  private double makeItQubicMeters(double volumeInMM) {
    return volumeInMM / 1000000000L;
  }

  /**
   * Helper method to generate the XML text belonging to the items in the collection.
   *
   * @param listOfItems The list of items to parse and generate the XML for.
   * @return The generated XML elements as text
   */
  private String

  getItemText(List<Item> listOfItems) {
    StringBuilder theResult = new StringBuilder("");
    if (listOfItems.size() > 0) {
      for (Item item : listOfItems) {
        if (item.getIsTopLevel().equalsIgnoreCase("Yes")) {
          theResult.append("\t\t<ItemCatalog>\n");
          theResult.append("\t\t\t<Item>").append(item.getItemID()).append("</Item>\n");
          theResult.append("\t\t\t<Revision>")
                   .append(item.getRevisionID())
                   .append("</Revision>\n");
          theResult.append("\t\t\t<Description>")
                   .append(item.getTitle())
                   .append("</Description>\n");
          String pattern = "yyyy-MM-dd";
          SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
          String date = simpleDateFormat.format(new Date());
          theResult.append("\t\t\t<SetupDate>")
                   .append(date)
                   .append("</SetupDate>\n");
          theResult.append("\t\t\t<User1>").append(item.getDescription()).append("</User1>\n");
          DecimalFormat df = new DecimalFormat("0.0000");
          df.setRoundingMode(RoundingMode.HALF_UP);
          String weight = getWeightIfAvailable(item.getOtherAttributes());
          if (weight.length() > 0) {
            theResult.append("\t\t\t<Weight>").append(weight).append("</Weight>\n");
          }
          theResult.append("\t\t\t<WeightUnitofMeasure>")
                   .append(allProperties.getProperty(WEIGHT_UNIT_OF_MEASURE))
                   .append("</WeightUnitofMeasure>\n");
          theResult.append(getItemPlanning(item));
          theResult.append("\t\t</ItemCatalog>\n");
        }
      }
    }
    return theResult.toString();
  }

  /**
   * Helper method to create the ItemPlanning part of the XML
   *
   * @param theItem The object to get the information from
   * @return A String representing the ItemPlanning information
   */
  private String getItemPlanning(Item theItem) {
    StringBuilder theResult = new StringBuilder("");
    theResult.append("\t\t\t<ItemPlanning>\n");
    theResult
            .append("\t\t\t\t<CorporationControlNumber>")
            .append(allProperties.get(CORPORATION_CONTROL_NUMBER))
            .append("</CorporationControlNumber>\n");
    theResult
            .append("\t\t\t\t<AccountingGroupID>")
            .append(allProperties.get(ACCOUNTING_GROUP_ID))
            .append("</AccountingGroupID>\n");
    theResult
            .append("\t\t\t\t<StandardCost>")
            .append(allProperties.get(STANDARD_COST))
            .append("</StandardCost>\n");
    theResult.append(getBuyingUnitofMeasure(theItem));
    theResult
            .append("\t\t\t\t<CostMethod>")
            .append(allProperties.get(COST_METHOD))
            .append("</CostMethod>\n");
    theResult.append("\t\t\t\t<DirectCostFlag>")
            .append(allProperties.get(DIRECT_COST_FLAG))
            .append("</DirectCostFlag>\n");
    theResult.append("\t\t\t\t<LotControlledFlag>")
            .append(allProperties.get(LOT_CONTROLLED_FLAG))
            .append("</LotControlledFlag>\n");
    if (isMakeItem(theItem)) {
      theResult.append("\t\t\t\t<MakeItemFlag>Y</MakeItemFlag>\n");
    } else {
      theResult.append("\t\t\t\t<MakeItemFlag/>\n");
    }
    theResult.append("\t\t\t\t<PhantomFlag></PhantomFlag>\n");
    theResult.append("\t\t\t\t<QAStatusCode>")
            .append(allProperties.get(QA_STATUS_CODE))
            .append("</QAStatusCode>\n");
    theResult
        .append("\t\t\t\t<SellUnitofMeasure>")
        .append(getGlobalUnitOfMeasure(theItem))
        .append("</SellUnitofMeasure>\n");
    theResult
        .append("\t\t\t\t<StockUnitofMeasure>")
        .append(getGlobalUnitOfMeasure(theItem))
        .append("</StockUnitofMeasure>\n");
    theResult.append("\t\t\t\t<StockItem>")
            .append(allProperties.get(STOCK_ITEM))
            .append("</StockItem>\n");
    theResult.append("\t\t\t\t<VATCode>")
            .append(allProperties.get(VAT_CODE))
            .append("</VATCode>\n");
    theResult.append("\t\t\t\t<PRPItemFlag>")
            .append(allProperties.get(PRP_ITEM_FLAG))
            .append("</PRPItemFlag>\n");
    theResult.append(getBillofMaterialHeader(theItem.getBillOfMaterials(), theItem));
    if (isMakeItem(theItem)) {
      theResult.append(getRoutingInformation(theItem.getBillOfMaterials(), theItem));
    }
    theResult.append("\t\t\t</ItemPlanning>\n");
    theResult.append(getItemPlanningAdditional(theItem));
    return theResult.toString();
  }

  private boolean isMakeItem(Item theItem) {
    boolean isMakeItem = true;
    OtherAttributes otherAttributes = theItem.getOtherAttributes();
    List<Attribute> attributes = otherAttributes.getAttributes();
    for(Attribute attribute: attributes) {
      if(attribute.getName().equalsIgnoreCase("BOM Structure") &&
         attribute.getValue().equalsIgnoreCase("Purchased")) {
        isMakeItem = false;
      }
    }
    return isMakeItem;
  }

  /**
   * Helper method to get the ItemPlanningAdditionalInformation
   *
   * @return A String representing the Item Planning Additional information
   */
  private String getItemPlanningAdditional(Item theItem) {
    StringBuilder theResult = new StringBuilder("");
    theResult.append("\t\t\t\t<ItemPlanningAdditional>\n");
    theResult.append("\t\t\t\t\t<CorporationControlNumber>")
            .append(allProperties.get(CORPORATION_CONTROL_NUMBER))
            .append("</CorporationControlNumber>\n");
    theResult
            .append("\t\t\t\t\t<SourceItemFlag>")
            .append(allProperties.get(SOURCE_ITEM_FLAG))
            .append("</SourceItemFlag>\n");
    theResult
            .append("\t\t\t\t\t<ASPCPOMasterLocation>")
            .append(allProperties.get(ASPCPO_MASTER_LOCATION))
            .append("</ASPCPOMasterLocation>\n");
    theResult
            .append("\t\t\t\t\t<StandardUM>")
            .append(getGlobalUnitOfMeasure(theItem))
            // .append(getUnitOfMeasure(theItem.getOtherAttributes()))
            .append("</StandardUM>\n");
    theResult.append("\t\t\t\t</ItemPlanningAdditional>\n");
    return theResult.toString();
  }

  /**
   * Helper method to get the RoutingInformation
   *
   * @return A String representing the Routing information
   */
  private String getRoutingInformation(List<BillOfMaterial> billOfMaterials, Item theItem) {
    String theKey = getCostCenter(theItem.getOtherAttributes());
    if (theKey.length() != 0) {
      StringBuilder theResult = new StringBuilder("");
      theResult.append("\t\t\t\t<RoutingHeader>\n");
      theResult.append("\t\t\t\t\t<BOMCostRoutingTypeCode>");
      theResult.append(allProperties.get(BOM_COST_ROUTING_TYPE_CODE));
      theResult.append("</BOMCostRoutingTypeCode>\n");
      theResult.append("\t\t\t\t\t<RoutingIncrement>");
      theResult.append(allProperties.get(ROUTING_INCREMENT));
      theResult.append("</RoutingIncrement>\n");
      theResult.append("\t\t\t\t\t<LastSequence>");
      theResult.append(allProperties.get(LAST_SEQUENCE));
      theResult.append("</LastSequence>\n");
      theResult.append("\t\t\t\t\t<RoutingDetail>\n");
      theResult.append("\t\t\t\t\t\t<OperationID>10</OperationID>\n");

      double theMachineFactor = 0.0;
      double theLaborFactor = 0.0;

      if (functionMap.get(theKey).getMachineFactor() != null) {
        String mFactor = replaceCommaWithPoint(functionMap.get(theKey).getMachineFactor());
        if (mFactor.length() > 0) {
          theMachineFactor = Double.parseDouble(mFactor);
        }
      } else {
        theMachineFactor = 0;
      }
      if (functionMap.get(theKey).getLaborFactor() != null) {
        String lFactor = replaceCommaWithPoint(functionMap.get(theKey).getLaborFactor());
        if (lFactor.length() > 0) {
          theLaborFactor = Double.parseDouble(lFactor);
        }
      } else {
        theLaborFactor = 0;
      }

      double totalMassValue = 0.0;
      String totalMassAsString = getWeightIfAvailable(theItem.getOtherAttributes());
      if (totalMassAsString.length() > 0) {
        totalMassValue = Double.parseDouble(totalMassAsString);
      } else {
        totalMassValue = 0.0;
      }
      Double machineRunTime = totalMassValue * theMachineFactor;
      Double laborRunTime = totalMassValue * theLaborFactor;
      boolean isMachineRuntimeAvailable = machineRunTime > 0.0;
      boolean isLaborRuntimeAvailable = laborRunTime > 0.0;
      theResult.append("\t\t\t\t\t\t<FunctionID>");
      theResult.append(functionMap.get(theKey).getFunction());
      theResult.append("</FunctionID>\n");
      theResult
          .append("\t\t\t\t\t\t<OperationType>")
          .append(functionMap.get(theKey).getType())
          .append("</OperationType>\n");
      DecimalFormat df = new DecimalFormat("0.0000");
      df.setRoundingMode(RoundingMode.HALF_UP);
      if (isMachineRuntimeAvailable && isLaborRuntimeAvailable) {
        String mccID = functionMap.get(theKey).getCostCenter();
        if (mccID != null && mccID.length() > 0) {
          theResult
              .append("\t\t\t\t\t\t<LaborCostCenterID>")
              .append(mccID)
              .append("</LaborCostCenterID>\n");
        }
        theResult
            .append("\t\t\t\t\t\t<LaborRunTime>")
            .append(df.format(laborRunTime))
            .append("</LaborRunTime>\n");

        theResult.append("\t\t\t\t\t\t<NumberOfMachines>1</NumberOfMachines>\n");
        if (mccID != null && mccID.length() > 0) {
          theResult
              .append("\t\t\t\t\t\t<MachineCostCenterID>")
              .append(mccID)
              .append("</MachineCostCenterID>\n");
        }
        theResult
            .append("\t\t\t\t\t\t<MachineRunTime>")
            .append(df.format(machineRunTime))
            .append("</MachineRunTime>\n");
        theResult.append("\t\t\t\t\t\t<PacerType>").append("B").append("</PacerType>\n");
        theResult.append("\t\t\t\t\t\t<RoutingUM>");
        theResult.append(allProperties.get(ROUTING_UM));
        theResult.append("</RoutingUM>\n");
      }
      if (isMachineRuntimeAvailable && !isLaborRuntimeAvailable) {
        theResult.append("\t\t\t\t\t\t<NumberOfMachines>1</NumberOfMachines>\n");
        String mccID = functionMap.get(theKey).getCostCenter();
        if (mccID != null && mccID.length() > 0) {
          theResult
              .append("\t\t\t\t\t\t<MachineCostCenterID>")
              .append(mccID)
              .append("</MachineCostCenterID>\n");
        }
        theResult
            .append("\t\t\t\t\t\t<MachineRunTime>")
            .append(df.format(machineRunTime))
            .append("</MachineRunTime>\n");
        theResult.append("\t\t\t\t\t\t<PacerType>").append("M").append("</PacerType>\n");
        theResult.append("\t\t\t\t\t\t<RoutingUM>");
        theResult.append(allProperties.get(ROUTING_UM));
        theResult.append("</RoutingUM>\n");
      }
      if (!isMachineRuntimeAvailable && isLaborRuntimeAvailable) {
        String mccID = functionMap.get(theKey).getCostCenter();
        if (mccID != null && mccID.length() > 0) {
          theResult
              .append("\t\t\t\t\t\t<LaborCostCenterID>")
              .append(mccID)
              .append("</LaborCostCenterID>\n");
        }
        theResult
            .append("\t\t\t\t\t\t<LaborRunTime>")
            .append(df.format(laborRunTime))
            .append("</LaborRunTime>\n");
        theResult.append("\t\t\t\t\t\t<NumberOfMachines>1</NumberOfMachines>\n");
        theResult.append("\t\t\t\t\t\t<PacerType>").append("L").append("</PacerType>\n");
        theResult.append("\t\t\t\t\t\t<RoutingUM>");
        theResult.append(allProperties.get(ROUTING_UM));
        theResult.append("</RoutingUM>\n");
      }
      if (!isMachineRuntimeAvailable && !isLaborRuntimeAvailable) {
        theResult.append("\t\t\t\t\t\t<LaborRunTime></LaborRunTime>\n");
        theResult.append("\t\t\t\t\t\t<NumberOfMachines></NumberOfMachines>\n");
        theResult.append("\t\t\t\t\t\t<MachineCostCenterID></MachineCostCenterID>\n");
        theResult.append("\t\t\t\t\t\t<MachineRunTime></MachineRunTime>\n");
        theResult.append("\t\t\t\t\t\t<PacerType></PacerType>\n");
        theResult.append("\t\t\t\t\t\t<RoutingUM></RoutingUM>\n");
      }
      String wcID = functionMap.get(theKey).getWorkCenter();
      if (wcID != null && wcID.length() > 0) {
        theResult.append("\t\t\t\t\t\t<WorkCenterID>");
        theResult.append(wcID);
        theResult.append("</WorkCenterID>\n");
      }

      theResult.append("\t\t\t\t\t</RoutingDetail>\n");
      theResult.append("\t\t\t\t</RoutingHeader>\n");
      return theResult.toString();
    } else {
      return "";
    }
  }

  private String getCostCenter(OtherAttributes otherAttributes) {
    List<Attribute> attributeList = otherAttributes.getAttributes();
    for(Attribute attribute: attributeList) {
      if(attribute.getName().equalsIgnoreCase("Cost_Center")) {
        String costCenter = attribute.getValue();
        if(!costCenter.isEmpty()) {
          return costCenter;
        }
      }
    }
    return "";
  }

  private String getUnitOfMeasure(OtherAttributes otherAttributes) {
    List<Attribute> attributeList = otherAttributes.getAttributes();
    for(Attribute attribute: attributeList) {
      if(attribute.getName().equalsIgnoreCase("Unit_Of_Measure")) {
        String unitOfMeasure = attribute.getValue();
        if(!unitOfMeasure.isEmpty()) {
          return unitOfMeasure;
        } else {
          return "ST";
        }
      }
    }
    return "";
  }

  /**
   * Helper method to get the BillOfMaterialInformation
   *
   * @param billOfMaterials The bill of material information
   * @param theItem The Parent object where we need to get some information from.
   * @return A String representing the Bill of Matrial information
   */
  private String getBillofMaterialHeader(List<BillOfMaterial> billOfMaterials, Item theItem) {
    if (billOfMaterials == null || billOfMaterials.isEmpty()) {
      return "";
    }
    StringBuilder theResult = new StringBuilder("");
    int billOfMaterialUserSequence = 1;
    for (BillOfMaterial bom : billOfMaterials) {
      theResult.append("\t\t\t\t<BillOfMaterialHeader>\n");
      theResult.append("\t\t\t\t\t<BCRTypeCode>");
      theResult.append(allProperties.get(BCR_TYPE_CODE));
      theResult.append("</BCRTypeCode>\n");
      theResult.append("\t\t\t\t\t<BillofMaterialDescription>FROM VAULT</BillofMaterialDescription>\n");
      theResult
              .append("\t\t\t\t\t<BillofMaterialIncrement>")
              .append(allProperties.get(BILL_OF_MATERIAL_INCREMENT))
              .append("</BillofMaterialIncrement>\n");
      theResult
              .append("\t\t\t\t\t<LastSequence>")
              .append(allProperties.get(LAST_SEQUENCE))
              .append("</LastSequence>\n");
      for (BillOfMaterialItem aBillOfMaterialItem : bom.getBillOfMaterials()) {
        theResult.append("\t\t\t\t\t<BillOfMaterialDetail>\n");
        theResult.append(getBillOfMaterailUserSequence(aBillOfMaterialItem, theItem, billOfMaterialUserSequence));
        billOfMaterialUserSequence++;
        theResult
            .append("\t\t\t\t\t\t<BalloonID>")
            .append(aBillOfMaterialItem.getPositionNumber())
            .append("</BalloonID>\n");
        theResult
            .append("\t\t\t\t\t\t<ComponentItem>")
            .append(aBillOfMaterialItem.getBillOfMaterialItemID())
            .append("</ComponentItem>\n");
        if (aBillOfMaterialItem.getItemQuantity() != null) {
          theResult
              .append("\t\t\t\t\t\t<ComponentItemQty>")
              .append(replaceCommaWithPoint(aBillOfMaterialItem.getItemQuantity()))
              .append("</ComponentItemQty>\n");
        }
        theResult
            .append("\t\t\t\t\t\t<ComponentItemRevision>")
            .append(aBillOfMaterialItem.getRevisionID())
            .append("</ComponentItemRevision>\n");
        theResult
                .append("\t\t\t\t\t\t<EffectiveBeginningDate>")
                .append(format.format(theItem.getRevisionReleasedDate()))
                .append("</EffectiveBeginningDate>\n");
        theResult.append("\t\t\t\t\t\t<PrintFlag>Y</PrintFlag>\n");
        // theResult.append("\t\t\t\t\t\t<WorkOrderCommitFlag>Y</WorkOrderCommitFlag>\n");
        theResult.append("\t\t\t\t\t</BillOfMaterialDetail>\n");
      }
      theResult.append("\t\t\t\t</BillOfMaterialHeader>\n");
    }
    return theResult.toString();
  }

  private String getBillOfMaterailUserSequence(
      BillOfMaterialItem aBillOfMaterialItem, Item theItem, int sequencenumber) {
    Attribute anAttribute = attributesContainRawMaterial(theItem.getOtherAttributes());
    if (anAttribute == null
        || anAttribute.getValue() == null
        || anAttribute.getValue().length() == 0) {
      return "\t\t\t\t\t\t<BillOfMaterailUserSequence>" + sequencenumber + "</BillOfMaterailUserSequence>\n";
    } else {
      return "\t\t\t\t\t\t<BillOfMaterailUserSequence>"
          + aBillOfMaterialItem.getRowOrder()
          + "</BillOfMaterailUserSequence>\n";
    }
  }

  /**
   * Helper method to transform from Autocad globalUnitofMeasure to Glovia xxxUnitofMeasure. Each ->
   * ST Millimeters -> MM Meters -> M Other values -> empty string
   *
   * @param theItem The item to get the Autocad GlobalUnitofMeasure from
   * @return the transformed value
   */
  private String getGlobalUnitOfMeasure(Item theItem) {
    switch (theItem.getGlobalUnitOfMeasure()) {
      case "Each":
//      case "EA" :
        return "EA";
      case "Millimeters":
      case "Millimeter":
        return "MM";
      case "Meters":
      case "Meter":
        return "M";
      default:
        return "UNKNOWN Unit Of Measure";
    }
  }

  private String getBuyingUnitofMeasure(Item theItem) {
    String unitOfMeasure = getGlobalUnitOfMeasure(theItem);
    return ("\t\t\t\t<BuyingUnitofMeasure>" + unitOfMeasure + "</BuyingUnitofMeasure>\n");
  }

  /**
   * Helper method to determine if there is an Attribute with a value of "RawMaterial"
   *
   * @param otherAttributes The Attributes to search in.
   * @return True and only true if there is an Attribute which has a name with "RawMaterial. False
   *     otherwise.
   */
  private Attribute attributesContainRawMaterial(OtherAttributes otherAttributes) {
    if (otherAttributes.containsAttributes()) {
      List<Attribute> theAttributes = otherAttributes.getAttributes();
      for (Attribute anAttribute : theAttributes) {
        if (anAttribute.getName().equalsIgnoreCase("RawMaterial")) {
          return anAttribute;
        }
      }
    }
    return null;
  }

  /**
   * Helper method to get the Weight if available
   *
   * @param otherAttributes The list of attributes that could contain an attribute with a name of
   *     'TotalMass'. If so create a String containing the value of the weight.
   * @return The XML element Weight if available
   */
  public String getWeightIfAvailable(OtherAttributes otherAttributes) {
    // Using deprecated constructor for compatibility with Java <19
    Locale.setDefault(new Locale("en", "US"));
    NumberFormat nf = NumberFormat.getNumberInstance(Locale.getDefault());
    DecimalFormat df = (DecimalFormat)nf;
    df.applyPattern("###.####");
//    String output = df.format(value);
//    System.out.println(pattern + " " + output + " " + loc.toString());
    if (otherAttributes.containsAttributes()) {
      List<Attribute> theAttributes = otherAttributes.getAttributes();
      for (Attribute anAttribute : theAttributes) {
        if (anAttribute.getName().equalsIgnoreCase("TotalMass")) {
          String theValue = anAttribute.getValue();
          theValue = replaceCommaWithPoint(theValue);
          if (theValue.length() > 0) {
            df.setRoundingMode(RoundingMode.HALF_UP);
            String formattedString = df.format(Double.parseDouble(theValue)/1000);
            return df.format(Double.parseDouble(formattedString));
          }
        }
      }
    }
    return "";
  }

  private String replaceCommaWithPoint(String theValue) {
    if(theValue == null) {
      return "";
    }
    if (theValue.contains(",")) {
      theValue = theValue.replace(",", ".");
    }
    return theValue;
  }

  /**
   * Method to process the OtherAttribute nodes
   *
   * @param someNode This is the parent Attachments node
   * @param itemNode This is the ItemNode where the attachments are part of.
   */
  private void handleOtherAttributes(Node someNode, Item itemNode) {
    if(isDebugOn) {
      System.out.println("Going over all OtherAttributes nodes.");
    }
    // first get the nodes attributes (Only 1)
    OtherAttributes otherAttributes = new OtherAttributes();
    NamedNodeMap nnm = someNode.getAttributes();
    if (nnm != null) {
      Node node = nnm.getNamedItem("groupLabel");
      if (node != null) {
        otherAttributes.setGroupLabel(node.getNodeValue());
        NodeList childNodes = someNode.getChildNodes();
        int numberOfChildNodes = childNodes.getLength();
        if(isDebugOn) {
          System.out.println("Going over all " + numberOfChildNodes + " OtherAttribute nodes.");
        }
        for (int index = 0; index < numberOfChildNodes; index++) {
          Node otherAttributeNode = childNodes.item(index);
          if (otherAttributeNode.getLocalName() == null
                  || !otherAttributeNode.getLocalName().equals("OtherAttribute")) {
            continue;
          }
          Attribute anAttribute = new Attribute();
          anAttribute.setName(otherAttributeNode.getAttributes().getNamedItem("name").getNodeValue());
          anAttribute.setValue(otherAttributeNode.getAttributes().getNamedItem("value").getNodeValue());
          anAttribute.setUnits(otherAttributeNode.getAttributes().getNamedItem("units").getNodeValue());
          anAttribute.setDataType(
                  otherAttributeNode.getAttributes().getNamedItem("dataType").getNodeValue());
          anAttribute.setDataTypeOtherDescription(
                  otherAttributeNode
                          .getAttributes()
                          .getNamedItem("dataTypeOtherDescription")
                          .getNodeValue());
          anAttribute.setDescription(
                  otherAttributeNode.getAttributes().getNamedItem("description").getNodeValue());
          otherAttributes.addAttributes(anAttribute);
        }
      }
    }
    itemNode.setOtherAttributes(otherAttributes);
  }

  /**
   * Method to process the BillOfMaterial nodes
   *
   * @param someNode This is the parent BillOfMaterial node
   * @param itemNode This is the ItemNode where the BillOfMaterial is part of.
   */
  private void handleBillOfMaterial(Node someNode, Item itemNode) {
    NodeList childNodes = someNode.getChildNodes();
    int numberOfChildNodes = childNodes.getLength();
    if(isDebugOn) {
      log.info("Going over all " + numberOfChildNodes + " BillOfMaterialItem nodes within this BillOfMaterial.");
    }
    BillOfMaterial bom = new BillOfMaterial();
    for (int index = 0; index < numberOfChildNodes; index++) {
      Node attachmentNode = childNodes.item(index);
      if (attachmentNode.getLocalName() == null
          || !attachmentNode.getLocalName().equals("BillOfMaterialItem")) {
        continue;
      }
      // Check to see if it is an item out of the existingParts table.
      // If so, skip it.
      String ID = attachmentNode.getAttributes().getNamedItem("billOfMaterialItemID").getNodeValue();
      String revID = attachmentNode.getAttributes().getNamedItem("revisionID").getNodeValue();
//      if(isItAnExistingPart(ID, revID)) {
//        continue;
//      }

      BillOfMaterialItem aBillOfMaterialItem = new BillOfMaterialItem();
      aBillOfMaterialItem.setBillOfMaterialItemID(
          attachmentNode.getAttributes().getNamedItem("billOfMaterialItemID").getNodeValue());
      aBillOfMaterialItem.setBillOfMaterialItemUniqueID(
          attachmentNode.getAttributes().getNamedItem("billOfMaterialItemUniqueID").getNodeValue());
      aBillOfMaterialItem.setBillOfMaterialType(
          attachmentNode.getAttributes().getNamedItem("billOfMaterialType").getNodeValue());
      aBillOfMaterialItem.setBillOfMaterialTypeOtherDescription(
          attachmentNode
              .getAttributes()
              .getNamedItem("billOfMaterialTypeOtherDescription")
              .getNodeValue());
      aBillOfMaterialItem.setBillOfMaterialUnitOfMeasure(
          attachmentNode
              .getAttributes()
              .getNamedItem("billOfMaterialUnitOfMeasure")
              .getNodeValue());
      aBillOfMaterialItem.setDescription(
          attachmentNode.getAttributes().getNamedItem("description").getNodeValue());
      aBillOfMaterialItem.setDetailID(
          attachmentNode.getAttributes().getNamedItem("detailID").getNodeValue());
      aBillOfMaterialItem.setInstanceCount(
          Byte.parseByte(
              attachmentNode.getAttributes().getNamedItem("instanceCount").getNodeValue()));
//      aBillOfMaterialItem.setItemQuantity(
//          attachmentNode.getAttributes().getNamedItem("itemQuantity").getNodeValue());
      aBillOfMaterialItem.setItemQuantity(
              attachmentNode.getAttributes().getNamedItem("instanceCount").getNodeValue());
      aBillOfMaterialItem.setNotes(
          attachmentNode.getAttributes().getNamedItem("notes").getNodeValue());
      aBillOfMaterialItem.setPositionNumber(
          Byte.parseByte(
              attachmentNode.getAttributes().getNamedItem("positionNumber").getNodeValue()));
      aBillOfMaterialItem.setRevisionID(
          attachmentNode.getAttributes().getNamedItem("revisionID").getNodeValue());
      aBillOfMaterialItem.setRowOrder(
          Byte.parseByte(attachmentNode.getAttributes().getNamedItem("rowOrder").getNodeValue()));
      aBillOfMaterialItem.setUnitSize(
          attachmentNode.getAttributes().getNamedItem("unitSize").getNodeValue());
      bom.addBillOfMaterialItem(aBillOfMaterialItem);
    }
    itemNode.addBillOfMaterial(bom);
  }

  /**
   * Method to process the Attachment nodes
   *
   * @param someNode This is the parent Attachments node
   * @param itemNode This is the ItemNode where the attachments are part of.
   */
  private void handleAttachments(Node someNode, Item itemNode) {
    NodeList childNodes = someNode.getChildNodes();
    int numberOfChildNodes = childNodes.getLength();
    if(isDebugOn) {
      log.info("Going over all " + numberOfChildNodes + " Attachment nodes.");
    }
    for (int index = 0; index < numberOfChildNodes; index++) {
      Node attachmentNode = childNodes.item(index);
      if (attachmentNode.getLocalName() == null
          || !attachmentNode.getLocalName().equals("Attachment")) {
        continue;
      }
      Attachment anAttachment = new Attachment();
      anAttachment.setAttachmentCreateDate(
          attachmentNode.getAttributes().getNamedItem("attachmentCreateDate").getNodeValue());
      anAttachment.setAttachmentModificationDate(
          attachmentNode.getAttributes().getNamedItem("attachmentModificationDate").getNodeValue());
      anAttachment.setDescription(
          attachmentNode.getAttributes().getNamedItem("description").getNodeValue());
      anAttachment.setCheckSum(
          attachmentNode.getAttributes().getNamedItem("checkSum").getNodeValue());
      anAttachment.setFileID(
          Integer.parseInt(attachmentNode.getAttributes().getNamedItem("fileID").getNodeValue()));
      anAttachment.setFileName(
          attachmentNode.getAttributes().getNamedItem("fileName").getNodeValue());
      anAttachment.setFileSize(
          Integer.parseInt(attachmentNode.getAttributes().getNamedItem("fileSize").getNodeValue()));
      anAttachment.setGlobalMimeType(
          attachmentNode.getAttributes().getNamedItem("globalMimeType").getNodeValue());
      anAttachment.setIsFileInPackage(
          attachmentNode.getAttributes().getNamedItem("isFileInPackage").getNodeValue());
      anAttachment.setUniversalResourceID(
          attachmentNode.getAttributes().getNamedItem("universalResourceID").getNodeValue());
      anAttachment.setVersionID(
          attachmentNode.getAttributes().getNamedItem("versionID").getNodeValue());
      itemNode.addAttachment(anAttachment);
    }
  }

  /**
   * Helper method to create a Date object from a given string that should represent a date The
   * String should have the following format: yyyy-mm-dd. This method should throw an exception if
   * it does not fulfil this requirement
   *
   * @param aDateString A string representing a date
   * @return A Date object set to the date the input String represented
   */
  private Date createDate(String aDateString) throws WrongDateFormatException {
    if (aDateString.matches("^\\d{4}-\\d{2}-\\d{2}$")) {
      Calendar cal = Calendar.getInstance();
      cal.set(Calendar.YEAR, Integer.parseInt(aDateString.substring(0, 4)));
      // Month starts counting a 0 value! So January = 0, February = 1 etc.
      cal.set(Calendar.MONTH, Integer.parseInt(aDateString.substring(6, 7)) - 1);
      cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(aDateString.substring(9, 10)));
      return new Date(cal.getTimeInMillis());
    } else {
      throw new WrongDateFormatException(
          "The given string does not fulfil the required format of yyyy-mm-dd. It is: "
              + aDateString);
    }
  }

  /** This method is going to create the requested XML */
  private void createNewXML(String xmlFileName) {
    if(isDebugOn) {
      log.info("Aantal elementen in originele bestand: " + pd.getItems().size());
    }
    List<Item> gefilterdeItems = verwijderExistingItems(pd.getItems());
    if(isDebugOn) {
      log.info("Aantal elementen na verwijdering existing parts: " + gefilterdeItems.size());
    }
    if(gefilterdeItems.isEmpty()) {
      return;
    }
    // Creating the new filename from the input file.
    String newFileName =
        allProperties.getProperty(DESTINATION_DIRECTORY)
            + File.separator
            + xmlFileName.substring(0, (xmlFileName.length() - 4))
            + "-Glovia.xml";
    File gloviaFile = new File(newFileName);
    try {
      FileWriter gloviaFileWriter = new FileWriter(gloviaFile);
      gloviaFileWriter.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
      gloviaFileWriter.write(
          "<Gapi xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.crescentone.com/gapi/MaterialItemMaster\" "
              + "source_reference=\""
              + allProperties.getProperty(SOURCE_REFERENCE)
              + "\" name=\"MaterialItemMaster\" destination_reference=\""
              + allProperties.getProperty(DESTINATION_REFERENCE)
              + "\">\n");
      gloviaFileWriter.write("\t<MaterialItemMaster>\n");
      gloviaFileWriter.write(
          "\t\t<SourceTradingPartner>"
              + allProperties.getProperty(SOURCE_TRADING_PARTNER)
              + "</SourceTradingPartner>\n");
      gloviaFileWriter.write(
          "\t\t<DestinationTradingPartner>"
              + allProperties.getProperty(DESTINATION_TRADING_PARTNER)
              + "</DestinationTradingPartner>\n");
      gloviaFileWriter.write("\t\t<ItemCatalogIncludedFlag>" + allProperties.getProperty(ITEM_CATALOG_INCLUDED_FLAG) + "</ItemCatalogIncludedFlag>\n");
      gloviaFileWriter.write("\t\t<ItemPlanningIncludedFlag>" + allProperties.getProperty(ITEM_PLANNING_INCLUDED_FLAG) + "</ItemPlanningIncludedFlag>\n");
      gloviaFileWriter.write("\t\t<BOMIncludedFlag>" + allProperties.getProperty(BOM_INCLUDED_FLAG) + "</BOMIncludedFlag>\n");
      gloviaFileWriter.write("\t\t<RoutingIncludedFlag>" + allProperties.getProperty(ROUTING_INCLUDED_FLAG) + "</RoutingIncludedFlag>\n");
      gloviaFileWriter.write(getItemText(gefilterdeItems));
      gloviaFileWriter.write("\t</MaterialItemMaster>\n");
      gloviaFileWriter.write("</Gapi>");
      gloviaFileWriter.flush();
      gloviaFileWriter.close();
      // Move the original file to new directory.
    } catch (IOException ioe) {
      log.warning(ioe.getMessage());
    }
  }

  private List<Item> verwijderExistingItems(List<Item> items) {
    List<Item> gefilterdeLijst = new ArrayList<>();
    for(Item anItem: items) {
      if(!anItem.isExistingPart()) {
        gefilterdeLijst.add(anItem);
      }
    }
    return gefilterdeLijst;
  }

  private boolean isItAnExistingPart(String id, String rev) {
    if(existingPartsMap.containsKey(id)) {
      String originalRevId = rev;
      if(originalRevId.equals("-") || originalRevId.equals("")) {
        originalRevId = " ";
      }
      String existingRevId = existingPartsMap.get(id).getRevision();
      if(existingRevId.equals("")) {
        existingRevId = " ";
      }
      if(existingRevId.equals(originalRevId)
              || existingRevId.equals("-")) {
        if(isDebugOn) {
          log.info("Existing part gevonden. ID: " + id + " revision:" + rev);
        }
        return true;
      }

    }
    return false;
  }

  /**
   * This method is needed for the windows service. It will check the given
   * directory(SOURCE_FILE_OR_DIRECTOR) to see if new files have arrived.
   *
   * @throws InterruptedException Will be thrown if we have to stop watching for some obscure reason
   * @throws IOException Same thing I think.
   */
  private void watchCadacDirectory() throws InterruptedException, IOException {
    WatchService watchService = FileSystems.getDefault().newWatchService();
    Path path = Paths.get(allProperties.getProperty(SOURCE_FILE_OR_DIRECTORY));

    path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);

    WatchKey key;

    while ((key = watchService.take()) != null) {
      List<WatchEvent<?>> owatchList = key.pollEvents();
      owatchList.forEach(
          (event) -> {
            letsStart();
          });
      key.reset();
    }
  }

  /** This method will print the version information to the commandline */
  private void showVersionInformation() {
    System.out.println(
        "nl.response.cadacparser.CadacParser version " + VERSION + " (c)2021 Response BV");
  }

  public static void main(String[] args)
      throws IOException, SAXException, ParserConfigurationException, InterruptedException {
    CadacParser cp = new CadacParser();

    if (args.length == 1) {
      switch (args[0]) {
        case "-?":
        case "-h":
        case "--help":
          cp.showHelpText();
          break;
        case "-v":
        case "--version":
          cp.showVersionInformation();
          break;
        default:
          break;
      }
    }

    allProperties = cp.readProperties();
    cp.readFunctionTableData();
    cp.readExistingPartsTable();
    cp.watchCadacDirectory(); // Deze uitcommentarieren als lokaal gaat testen.
    // cp.letsStart(); // Alleen voor lokaal testen.
  }

  private void readFunctionTableData() {
    System.out.println("Functietabel lezen");
    File functionsTableFile = new File(allProperties.getProperty(FUNCTION_TABLE_LOCATION));
    if (functionsTableFile.exists()) {
      try {
        FileReader fr = new FileReader(functionsTableFile);
        BufferedReader br = new BufferedReader(fr);
        String aLine = null;
        while ((aLine = br.readLine()) != null) {
          String[] lineParts = parseTheLine(aLine);
          functionMap.put(
              lineParts[3],
              new FunctionTableRow(
                  lineParts[0],
                  lineParts[1],
                  lineParts[2],
                  lineParts[3],
                  lineParts[4],
                  lineParts[5],
                  lineParts[6],
                  lineParts[7]));
        }
        br.close();
        fr.close();
      } catch (IOException e) {
        log.warning(
            "Het lezen van de functietabel is niet goed gegaan. Is het juiste pad gezet? (Zie: functionTableLocation in het properties bestand)");
        e.printStackTrace();
      }
    } else {
        log.warning("Het functie-tabel bestand bestaat niet of is niet te lezen! Het omzetten zal niet goed gaan!");
      }
  }

  private String[] parseTheLine(String aLine) {
    String[] theParts = new String[8];
    int index = 0;
    for (int counter = 0; counter < 7; counter++) {
      int nextPart = aLine.indexOf(";", index);
      String somePart = aLine.substring(index, nextPart);
      theParts[counter] = somePart;
      index = nextPart + 1;
    }
    String temp = aLine.substring(aLine.lastIndexOf(";"));
    if(temp.length() == 1) {
      theParts[7] = "";
    } else {
      theParts[7] = temp.substring(1);
    }
    return theParts;
  }


  private void readExistingPartsTable() {
    System.out.println("Bestaande onderdelen inlezen");
    File ReportTableFile = new File("./ExistingParts.csv");
    if (ReportTableFile.exists()) {
      try {
        FileReader fr = new FileReader(ReportTableFile);
        BufferedReader br = new BufferedReader(fr);
        String aLine = null;
        while ((aLine = br.readLine()) != null) {
          if(aLine.trim().length() == 0) {
            continue;
          }
          String[] lineParts = parsePartsLine(aLine);
          existingPartsMap.put(
                  lineParts[2],
                  new PartRow(
                          lineParts[0],
                          lineParts[1],
                          lineParts[2],
                          lineParts[3],
                          lineParts[4],
                          lineParts[5],
                          lineParts[6],
                          lineParts[7],
                          lineParts[8],
                          lineParts[9]));
        }
        br.close();
        fr.close();
        System.out.println(existingPartsMap.size() + " onderdelen ingelezen");
      } catch (IOException e) {
        log.warning(
                "Het lezen van het bestaande onderdelen bestand is niet goed gegaan. Is het juiste pad gezet? ");
        e.printStackTrace();
      }
    } else {
      log.warning("Het bestaande onderdelen bestand bestaat niet of is niet te lezen! Het omzetten zal niet goed gaan!");
    }
  }


  private String[] parsePartsLine(String aLine) {
    String[] theParts = new String[10];
    int index = 0;
    for (int counter = 0; counter < 9; counter++) {
      int nextPart = aLine.indexOf(";", index);
      String somePart = aLine.substring(index, nextPart);
      //There might be quotes around the text. They must be removed.
      somePart = removeQuotesIfAvailble(somePart);
      theParts[counter] = somePart;
      index = nextPart + 1;
    }
    String temp = aLine.substring(aLine.lastIndexOf(";"));
    if(temp.length() == 1) {
      theParts[9] = "";
    } else {
      theParts[9] = temp.substring(1);
    }
    return theParts;
  }

  private String removeQuotesIfAvailble(String somePart) {
    String result = somePart;
    if(somePart.startsWith("\"")) {
      result = somePart.substring(1);
    }
    if(result.endsWith("\"")) {
      result = result.substring(0, result.length() - 1);
    }
    if(result.startsWith("\"") || result.endsWith("\"")) {
      result = removeQuotesIfAvailble(result);
    }
    return result;
  }

  private Properties readProperties() {
    System.out.println("Properties lezen");
    Properties props = new Properties();
    try {
      File thePropsFile = new File(INSTALL_DIRECTORY + File.separator + "cadac-parser.properties");
      if (!thePropsFile.exists()) {
        createDefaultPropsFileAndQuit();
      }
      FileReader fr = new FileReader(thePropsFile);
      props.load(fr);
      // nu kijken of alle benodigde properties aanwezig zijn
      Enumeration<?> theProps = props.propertyNames();
      while (theProps.hasMoreElements()) {
        String aProp = (String) theProps.nextElement();
        String tempPropName = aProp;
        if(!aProp.equals(aProp.toLowerCase(Locale.ROOT))) {
          tempPropName = aProp.toLowerCase(Locale.ROOT);
          props.put(tempPropName, props.get(aProp));
          props.remove(aProp);
        }
        checkProperty(props, tempPropName);
      }
    } catch (IOException e) {
      System.err.println("There is a problem with the property file.");
      System.err.println(
          "Does it have the correct name: 'cadac-parser.properties' and is it located");
      System.err.println("in the directory next to cadac-parser.jar file?");
      System.err.println(-1);
    }
    return props;
  }

  private void checkOutputDirectory(File outputDirectory) {
    if (!outputDirectory.exists()) {
      boolean isDirCreated = outputDirectory.mkdir();
      if (!isDirCreated) {
        System.err.println(
            outputDirectory.getAbsolutePath() + " does not exist and can't be created.");
        System.err.println(
            "Maybe a problem with access rights?. Are you allowed to create a directory?.");
        System.err.println(
            "Maybe define another destination directory in the property file and try again.");
        System.err.println(
            "If you are using this as a service you must restart or start the service again. It is broken down.");
        System.err.println(-5);
      }
    }
  }

  private void checkProperty(Properties props, String aProp) {
    boolean isFound = false;
    switch (aProp) {
      case SOURCE_FILE_OR_DIRECTORY:
      case DESTINATION_DIRECTORY:
      case FUNCTION_TABLE_LOCATION:
      case PROCESSED_DIRECTORY:
      case ERROR_DIRECTORY:
      case LAST_SEQUENCE:
      case SOURCE_REFERENCE:
      case DESTINATION_REFERENCE:
      case SOURCE_TRADING_PARTNER:
      case DESTINATION_TRADING_PARTNER:
      case ITEM_CATALOG_INCLUDED_FLAG:
      case ITEM_PLANNING_INCLUDED_FLAG:
      case BOM_INCLUDED_FLAG:
      case ROUTING_INCLUDED_FLAG:
      case WEIGHT_UNIT_OF_MEASURE:
      case CORPORATION_CONTROL_NUMBER:
      case ACCOUNTING_GROUP_ID:
      case STANDARD_COST:
      case BUYING_UNIT_OF_MEASURE:
      case COST_METHOD:
      case DIRECT_COST_FLAG:
      case LOT_CONTROLLED_FLAG:
      case QA_STATUS_CODE:
      case SELL_UNIT_OF_MEASURE:
      case STOCK_UNIT_OF_MEASURE:
      case STOCK_ITEM:
      case VAT_CODE:
      case PRP_ITEM_FLAG:
      case BCR_TYPE_CODE:
      case BILL_OF_MATERIAL_INCREMENT:
      case BOM_COST_ROUTING_TYPE_CODE:
      case ROUTING_INCREMENT:
      case SOURCE_ITEM_FLAG:
      case ASPCPO_MASTER_LOCATION:
      case STANDARD_UM:
      case ROUTING_UM:
      case DEBUG:
        checkPropertyValue(aProp, props);
        isFound = true;
        log.info(aProp + " is available with value: " + props.get(aProp));
        break;
      default:
        // De gevonden property is niet nodig
        System.err.println("This property is obsolete: " + aProp);
        System.err.println("We skip it and continue.");
        System.err.println("Please remove it from the property file.");
        break;
    }
    if (!isFound) {
      handlePropertyNotAvailable(aProp);
    }
  }

  private void createDefaultPropsFileAndQuit() {
    File thePropsFile = new File(INSTALL_DIRECTORY + File.separator + "cadac-parser.properties");
    try {
      FileWriter fw = new FileWriter(thePropsFile);
      fw.write(
          "# Windows users, pay attention! If using a path with backslashes add an extra backslash (So 2 instead of 1).\n");
      fw.write("\n");
      fw.write(
          "# This is the file to transform or the directory containing the files to transform.\n");
      fw.write(SOURCE_FILE_OR_DIRECTORY + "=\n");
      fw.write("# This is the directory the transformed file(s) will be placed in.\n");
      fw.write(DESTINATION_DIRECTORY + "=\n");
      fw.write("# This is the file that contains the function value that can be mapped to work center, cost center, standard operation, type, machine factor and laborFactor.\n");
      fw.write(FUNCTION_TABLE_LOCATION + "=FunctionTable-cadac.csv\n");
      fw.write("# This is the directory the processed file(s) will be placed in.\n");
      fw.write(PROCESSED_DIRECTORY + "=\n");
      fw.write("# This is the directory the error file(s) will be placed in.\n");
      fw.write(ERROR_DIRECTORY + "=\n");
      fw.write("# The settings that need to be set for it to work (Default values).\n");
      fw.write("#Diverse\n");
      fw.write(LAST_SEQUENCE + "=2000\n");
      fw.write(SOURCE_REFERENCE + "=Vault\n");
      fw.write(DESTINATION_REFERENCE + "=DUMMY\n");
      fw.write("#MaterialItemMaster\n");
      fw.write(SOURCE_TRADING_PARTNER + "=Vault\n");
      fw.write(DESTINATION_TRADING_PARTNER + "=DUMMY\n");
      fw.write(ITEM_CATALOG_INCLUDED_FLAG + "=Y\n");
      fw.write(ITEM_PLANNING_INCLUDED_FLAG + "=Y\n");
      fw.write(BOM_INCLUDED_FLAG + "=Y\n");
      fw.write(ROUTING_INCLUDED_FLAG + "=Y\n");
      fw.write("#ItemCatalog\n");
      fw.write(WEIGHT_UNIT_OF_MEASURE + "=KG\n");
      fw.write("ItemPlanning\n");
      fw.write(CORPORATION_CONTROL_NUMBER + "=WB\n");
      fw.write(ACCOUNTING_GROUP_ID + "=OHWG\n");
      fw.write(STANDARD_COST + "=123456\n");
      fw.write(BUYING_UNIT_OF_MEASURE + "=EA\n");
      fw.write(COST_METHOD + "=STD\n");
      fw.write(DIRECT_COST_FLAG + "=Y\n");
      fw.write(LOT_CONTROLLED_FLAG + "=Y\n");
      fw.write(QA_STATUS_CODE + "=3\n");
      fw.write(SELL_UNIT_OF_MEASURE + "=EA\n");
      fw.write(STOCK_UNIT_OF_MEASURE + "=EA\n");
      fw.write(STOCK_ITEM + "=Y\n");
      fw.write(VAT_CODE + "=H\n");
      fw.write(PRP_ITEM_FLAG + "=Y\n");
      fw.write("#BOM\n");
      fw.write(BCR_TYPE_CODE + "=CUR\n");
      fw.write(BILL_OF_MATERIAL_INCREMENT + "=10\n");
      fw.write("#Routing\n");
      fw.write(BOM_COST_ROUTING_TYPE_CODE + "=CUR\n");
      fw.write(ROUTING_INCREMENT + "=10\n");
      fw.write("#ItemPlanningAdditional\n");
      fw.write(SOURCE_ITEM_FLAG + "=Y\n");
      fw.write(ASPCPO_MASTER_LOCATION + "=BDH\n");
      fw.write(STANDARD_UM + "=KG\n");
      fw.write(DEBUG + "=false\n");
      fw.flush();
      fw.close();
      System.out.println(
          "We could not find a property file so we created one for you(cadac-parser.properties).");
      System.out.println("Open the property file and add the needed information.");
    } catch (IOException e) {
      e.printStackTrace();
    }

    System.err.println(-4);
  }

  private void checkPropertyValue(String key, Properties props) {
    String value = props.getProperty(key);
    if (value == null || value.length() == 0) {
      System.err.println("The next property does not have a (valid) value: " + key);
      System.err.println("Check the property file and give the property a correct value.");
      System.err.println(-3);
    }
  }

  private void handlePropertyNotAvailable(String missingProp) {
    System.err.println("The following property is missing from the property file: " + missingProp);
    System.err.println("Check the property file and add the property and its value.");
    System.err.println("Make sure the spelling is correct.");
    System.err.println(-2);
  }

  private void finishErrorMelding() {
    System.out.println("Is the property file correctly filled?");
    System.out.println(
        "Start the application with 'java -jar cadac-parser.jar -h for more information");
  }

  /**
   * Helper method to print a manual on the commandline Placed all the way to the bottom because it
   * only generates text.
   */
  private void showHelpText() {
    System.out.println(
        "This tool is used to transform a given CAD XML file into a Glovia XML file.");
    System.out.println("The generated file will be created at the location of this source file");
    System.out.println();
    System.out.println("Usage: java -jar cadac-parser.jar");
    System.out.println();
    System.out.println("System requirements: Java 1.8 needs to be installed.");
    System.out.println();
    System.out.println("How does it work.");
    System.out.println(
        "\tAll the XML files in the so called SOURCE_FILE_OR_DIRECTORY directory are read and ");
    System.out.println("\tordered on modification date with the oldest files first.");
    System.out.println("\tThen each file is parsed and a new file is generated.");
    System.out.println(
        "\tIf the original files is parsed successfully it will be moved to the so called PROCESSED_DIRECTORY directory");
    System.out.println(
        "\tIf the parsing of the original file is not successful the original file will be moved to the so called ERROR_DIRECTORY directory");
    System.out.println(
        "\tFor some items we need a lookup table. We can find the description, work center, cost center, standard operation, type and 2 factors by the function.");
    System.out.println(
        "\tThis is a ';' separated file. The location of this file is stored in FUNCTIONTABLE_LOCATION");
    System.out.println(
        "\tThe newly generated file will be placed in the DESTINATION_DIRECTORY directory.");
    System.out.println(
        "\tThe values for the mentioned directories can and must be set in the property file.");
    System.out.println();
    System.out.println(
        "Starting the application will generate a property file that needs to be filled");
    System.out.println(
        "with proper information. Read the generated property file for more information.");
    System.out.println("The following properties need to be available in the property file:");
    System.out.println(
        "  SOURCE_FILE_OR_DIRECTORY - This is where the file to transform should be located.");
    System.out.println(
        "  DESTINATION_DIRECTORY - This is where the newly created files need to be placed.");
    System.out.println(
        "  PROCESSED_DIRECTORY - This is where the processed file will be moved to.");
    System.out.println("  LAST_SEQUENCE - What value to use here. Default is '2000'");
    System.out.println("  SOURCE_TRADING_PARTNER - What value to use here. Default is 'Vault'");
    System.out.println(
        "  DESTINATION_TRADING_PARTNER - What value to use here. Default is 'DUMMY'");
    System.out.println("  CORPORATION_CONTROL_NUMBER - What value to use here. Default is 'WB'");
    System.out.println("  STANDARD_COST - What value to use here. Default is '123456'");
    System.out.println("  ACCOUNTING_GROUP_ID - What value to use here. Default is 'AAAA'");
    System.out.println("  COST_METHOD - What value to use here. Default is 'STD'");
    System.out.println("  BCR_TYPE_COD - What value to use here. Default is 'CUR'");
    System.out.println("  SOURCE_REFERENCE - What value to use here. Default is 'Vault'");
    System.out.println("  DESTINATION_REFERENCE - What value to use here. Default is 'DUMMY'");
    System.out.println("  BILL_OF_MATERIAL_INCREMENT - What value to use here. Default is '10'");
    System.out.println("  ROUTING_INCREMENT - What value to use here. Default is '10'");
    System.out.println("  SOURCE_ITEM_FLAG - What value to use here. Default is 'Y'");
    System.out.println("  ASPCPO_MASTER_LOCATION - What value to use here. Default is 'BDH'");
    System.out.println("  STANDARD_UM - What value to use here. Default is 'KG'");
    System.out.println();
    System.out.println("Special input");
    System.out.println();
    System.out.println(
        "\tInstead of giving <TheFileToParse> you could enter some special characters like:");
    System.out.println();
    System.out.println("\t-?, -h or --help");
    System.out.println("\tWill print out this help text.");
    System.out.println();
    System.out.println("\t-v or --version");
    System.out.println(
        "\tWill print out the version of this application. It could be useful if you need support");
    System.out.println();
    System.out.println("Need more help?");
    System.out.println("Send an e-mail to info@response.nl.");
    System.out.println();
  }
}
