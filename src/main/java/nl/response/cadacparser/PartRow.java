package nl.response.cadacparser;

public class PartRow {
    private String ccn;
    private String itemGroup;
    private String item;
    private String revision;
    private String description;
    private String setup;
    private String gtc;
    private String hpl;
    private String pc;
    private String planner;

    public PartRow(String ccn, String itemGroup, String item, String revision, String description, String setup, String gtc, String hpl, String pc, String planner) {
        this.ccn = ccn;
        this.itemGroup = itemGroup;
        this.item = item;
        this.revision = revision;
        this.description = description;
        this.setup = setup;
        this.gtc = gtc;
        this.hpl = hpl;
        this.pc = pc;
        this.planner = planner;
    }

    public PartRow(){}

    public String getCcn() {
        return ccn;
    }

    public void setCcn(String ccn) {
        this.ccn = ccn;
    }

    public String getItemGroup() {
        return itemGroup;
    }

    public void setItemGroup(String itemGroup) {
        this.itemGroup = itemGroup;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSetup() {
        return setup;
    }

    public void setSetup(String setup) {
        this.setup = setup;
    }

    public String getGtc() {
        return gtc;
    }

    public void setGtc(String gtc) {
        this.gtc = gtc;
    }

    public String getHpl() {
        return hpl;
    }

    public void setHpl(String hpl) {
        this.hpl = hpl;
    }

    public String getPc() {
        return pc;
    }

    public void setPc(String pc) {
        this.pc = pc;
    }

    public String getPlanner() {
        return planner;
    }

    public void setPlanner(String planner) {
        this.planner = planner;
    }

    @Override
    public String toString() {
        return "PartRow{" +
                "ccn='" + ccn + '\'' +
                ", itemGroup='" + itemGroup + '\'' +
                ", item='" + item + '\'' +
                ", revision='" + revision + '\'' +
                ", description='" + description + '\'' +
                ", setup='" + setup + '\'' +
                ", gtc='" + gtc + '\'' +
                ", hpl='" + hpl + '\'' +
                ", pc='" + pc + '\'' +
                ", planner='" + planner + '\'' +
                '}';
    }
}
