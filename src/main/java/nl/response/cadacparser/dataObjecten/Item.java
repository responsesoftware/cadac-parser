package nl.response.cadacparser.dataObjecten;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Item {

    private String itemID;
    private String itemUniqueID;
    private String lifeCycleDefnName;
    private String globalLifeCyclePhase;
    private String globalLifeCyclePhaseOtherDescription;
    private String globalProductType;
    private String itemClassification;
    private String revisionID;
    private String category;
    private String globalUnitOfMeasure;
    private String globalAccuracy;
    private int globalMinQuantity;
    private String makeBuy;
    private String makeBuyOtherDescription;
    private Date revisionReleasedDate;
    private String ownerName;
    private int ownerContactID;
    private String isTopLevel;
    private String description;
    private String effectivity;
    private String effectivityStart;
    private String effectivityEnd;
    private String title;
    private List<Attachment> attachmentsList = new ArrayList<>();
    private OtherAttributes otherAttributes = new OtherAttributes();
    private List<BillOfMaterial> billOfMaterialList = new ArrayList<>();
    private boolean isExistingPart = false;

    public void addBillOfMaterial(BillOfMaterial billOfMaterial) {
        billOfMaterialList.add(billOfMaterial);
    }

    public List<BillOfMaterial> getBillOfMaterials() {
        return billOfMaterialList;
    }

    public OtherAttributes getOtherAttributes() {
        return otherAttributes;
    }

    public void setOtherAttributes(OtherAttributes otherAttributes) {
        this.otherAttributes = otherAttributes;
    }

    public void addAttachment(Attachment attachment) {
        attachmentsList.add(attachment);
    }

    public List<Attachment> getAttachements() {
        return attachmentsList;
    }

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public String getItemUniqueID() {
        return itemUniqueID;
    }

    public void setItemUniqueID(String itemUniqueID) {
        this.itemUniqueID = itemUniqueID;
    }

    public String getLifeCycleDefnName() {
        return lifeCycleDefnName;
    }

    public void setLifeCycleDefnName(String lifeCycleDefnName) {
        this.lifeCycleDefnName = lifeCycleDefnName;
    }

    public String getGlobalLifeCyclePhase() {
        return globalLifeCyclePhase;
    }

    public void setGlobalLifeCyclePhase(String globalLifeCyclePhase) {
        this.globalLifeCyclePhase = globalLifeCyclePhase;
    }

    public String getGlobalLifeCyclePhaseOtherDescription() {
        return globalLifeCyclePhaseOtherDescription;
    }

    public void setGlobalLifeCyclePhaseOtherDescription(String globalLifeCyclePhaseOtherDescription) {
        this.globalLifeCyclePhaseOtherDescription = globalLifeCyclePhaseOtherDescription;
    }

    public String getGlobalProductType() {
        return globalProductType;
    }

    public void setGlobalProductType(String globalProductType) {
        this.globalProductType = globalProductType;
    }

    public String getItemClassification() {
        return itemClassification;
    }

    public void setItemClassification(String itemClassification) {
        this.itemClassification = itemClassification;
    }

    public String getRevisionID() {
        if(revisionID.equals("-")) {
            return "";
        }
        return revisionID;
    }

    public void setRevisionID(String revisionID) {
        this.revisionID = revisionID;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getGlobalUnitOfMeasure() {
        return globalUnitOfMeasure;
    }

    public void setGlobalUnitOfMeasure(String globalUnitOfMeasure) {
        this.globalUnitOfMeasure = globalUnitOfMeasure;
    }

    public String getGlobalAccuracy() {
        return globalAccuracy;
    }

    public void setGlobalAccuracy(String globalAccuracy) {
        this.globalAccuracy = globalAccuracy;
    }

    public int getGlobalMinQuantity() {
        return globalMinQuantity;
    }

    public void setGlobalMinQuantity(int globalMinQuantity) {
        this.globalMinQuantity = globalMinQuantity;
    }

    public String getMakeBuy() {
        if(makeBuy.equalsIgnoreCase("make")) {
            return "Y";
        } else {
            return "";
        }
    }

    public void setMakeBuy(String makeBuy) {
        this.makeBuy = makeBuy;
    }

    public String getMakeBuyOtherDescription() {
        return makeBuyOtherDescription;
    }

    public void setMakeBuyOtherDescription(String makeBuyOtherDescription) {
        this.makeBuyOtherDescription = makeBuyOtherDescription;
    }

    public Date getRevisionReleasedDate() {
        return revisionReleasedDate;
    }

    public void setRevisionReleasedDate(Date revisionReleasedDate) {
        this.revisionReleasedDate = revisionReleasedDate;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public int getOwnerContactID() {
        return ownerContactID;
    }

    public void setOwnerContactID(int ownerContactID) {
        this.ownerContactID = ownerContactID;
    }

    public String getIsTopLevel() {
        return isTopLevel;
    }

    public void setIsTopLevel(String isTopLevel) {
        this.isTopLevel = isTopLevel;
    }

    public String getDescription() {
        if(description != null && description.length() > 10) {
            return description.substring(0,10);
        }
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEffectivity() {
        return effectivity;
    }

    public void setEffectivity(String effectivity) {
        this.effectivity = effectivity;
    }

    public String getEffectivityStart() {
        return effectivityStart;
    }

    public void setEffectivityStart(String effectivityStart) {
        this.effectivityStart = effectivityStart;
    }

    public String getEffectivityEnd() {
        return effectivityEnd;
    }

    public void setEffectivityEnd(String effectivityEnd) {
        this.effectivityEnd = effectivityEnd;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String getAttachmentsAsString() {
        StringBuilder itemString = new StringBuilder();
        for (Attachment attachment : attachmentsList) {
            itemString.append("\t\t").append(attachment.toString()).append("\n");
        }
        return itemString.toString();
    }

    private String getOtherAttributesAsString() {
        StringBuilder itemString = new StringBuilder("\t\t\t" + otherAttributes.getGroupLabel() + "\n");
        List<Attribute> attributeList = otherAttributes.getAttributes();
        for (Attribute attribute : attributeList) {
            itemString.append("\t\t").append(attribute.toString()).append("\n");
        }
        return itemString.toString();
    }

    private String getBillOfMaterialListAsString() {
        StringBuilder itemString = new StringBuilder();
        for (BillOfMaterial billOfMaterial : billOfMaterialList) {
            itemString.append("\t\tBillOfMaterials{\n");
            for (BillOfMaterialItem bomItem : billOfMaterial.getBillOfMaterials()) {
                itemString.append("\t\t\t").append(bomItem.toString()).append("\n");
            }
            itemString.append("\t\t}\n");
        }
        return itemString.toString();
    }

    public boolean isExistingPart() {
        return isExistingPart;
    }

    public void setExistingPart(boolean existingPart) {
        isExistingPart = existingPart;
    }

    @Override
    public String toString() {
        return "Item{" + "\n" +
                "\t\titemID='" + itemID + '\'' + "\n" +
                "\t\titemUniqueID='" + itemUniqueID + '\'' + "\n" +
                "\t\tlifeCycleDefnName='" + lifeCycleDefnName + '\'' + "\n" +
                "\t\tglobalLifeCyclePhase='" + globalLifeCyclePhase + '\'' + "\n" +
                "\t\tglobalLifeCyclePhaseOtherDescription='" + globalLifeCyclePhaseOtherDescription + '\'' + "\n" +
                "\t\tglobalProductType='" + globalProductType + '\'' + "\n" +
                "\t\titemClassification='" + itemClassification + '\'' + "\n" +
                "\t\trevisionID='" + revisionID + '\'' + "\n" +
                "\t\tcategory='" + category + '\'' + "\n" +
                "\t\tglobalUnitOfMeasure='" + globalUnitOfMeasure + '\'' + "\n" +
                "\t\tlobalAccuracy='" + globalAccuracy + '\'' + "\n" +
                "\t\tglobalMinQuantity='" + globalMinQuantity + '\'' + "\n" +
                "\t\tmakeBuy='" + makeBuy + '\'' + "\n" +
                "\t\tmakeBuyOtherDescription='" + makeBuyOtherDescription + '\'' + "\n" +
                "\t\trevisionReleasedDate='" + revisionReleasedDate + '\'' + "\n" +
                "\t\townerName='" + ownerName + '\'' + "\n" +
                "\t\townerContactID='" + ownerContactID + '\'' + "\n" +
                "\t\tisTopLevel='" + isTopLevel + '\'' + "\n" +
                "\t\tdescription='" + description + '\'' + "\n" +
                "\t\teffectivity='" + effectivity + '\'' + "\n" +
                "\t\teffectivityStart='" + effectivityStart + '\'' + "\n" +
                "\t\teffectivityEnd='" + effectivityEnd + '\'' + "\n" +
                "\t\ttitle='" + title + '\'' + "\n" +
                "\t\tisExistingPart='" + isExistingPart + '\'' + "\n" +
                "\t\tOtherAttributes {\n" + getOtherAttributesAsString() +
                "\t\t}\n" +
                "\t\tBillOfMaterial {\n" + getBillOfMaterialListAsString() +
                "\t\t}\n" +
                "\t\tAttachments {\n" + getAttachmentsAsString() +
                "\t\t}\n" +
                "\t}";
    }
}
