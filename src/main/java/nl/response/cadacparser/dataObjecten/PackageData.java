package nl.response.cadacparser.dataObjecten;

import java.util.ArrayList;
import java.util.List;

public class PackageData {
  private String documentID;
  private String documentCreateDate;
  private String documentModificationDate;
  private String originatorByName;
  private String setOriginatedByID;
  private String description;
  private final List<Item> items = new ArrayList<>();

  public String getDocumentID() {
    return documentID;
  }

  public void setDocumentID(String documentID) {
    this.documentID = documentID;
  }

  public String getDocumentCreateDate() {
    return documentCreateDate;
  }

  public void setDocumentCreateDate(String documentCreateDate) {
    this.documentCreateDate = documentCreateDate;
  }

  public String getDocumentModificationDate() {
    return documentModificationDate;
  }

  public void setDocumentModificationDate(String documentModificationDate) {
    this.documentModificationDate = documentModificationDate;
  }

  public String getOriginatorByName() {
    return originatorByName;
  }

  public void setOriginatorByName(String originatorByName) {
    this.originatorByName = originatorByName;
  }

  public String getSetOriginatedByID() {
    return setOriginatedByID;
  }

  public void setSetOriginatedByID(String setOriginatedByID) {
    this.setOriginatedByID = setOriginatedByID;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void addItem(Item item) {
    items.add(item);
  }

  public List<Item> getItems() {
    return items;
  }

  private String getItemsAsString() {
    StringBuilder itemString = new StringBuilder();
    for (Item item : items) {
      itemString.append("\t").append(item.toString()).append("\n");
    }
    return itemString.toString();
  }

  public String toString() {
    return "PackageData{\n"
        + "documentID= " + documentID + "\n"
        + "documentCreateDate= " + documentCreateDate + "\n"
        + "documentModificationDate= "+ documentModificationDate + "\n"
        + "originatorByName= "+ originatorByName + "\n"
        + "originatorByID= " + setOriginatedByID + "\n"
        + "description= " + description + "\n"
        + "items: \n"
        + getItemsAsString() + "\n"
        + '}';
  }
}
