package nl.response.cadacparser.dataObjecten;

public class Attachment {

    private String fileName;
    private String universalResourceID;
    private int fileID;
    private String versionID;
    private int fileSize;
    private String checkSum;
    private String isFileInPackage;
    private String description;
    private String globalMimeType;
    private String attachmentCreateDate;
    private String attachmentModificationDate;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getUniversalResourceID() {
        return universalResourceID;
    }

    public void setUniversalResourceID(String universalResourceID) {
        this.universalResourceID = universalResourceID;
    }

    public int getFileID() {
        return fileID;
    }

    public void setFileID(int fileID) {
        this.fileID = fileID;
    }

    public String getVersionID() {
        return versionID;
    }

    public void setVersionID(String versionID) {
        this.versionID = versionID;
    }

    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public String getCheckSum() {
        return checkSum;
    }

    public void setCheckSum(String checkSum) {
        this.checkSum = checkSum;
    }

    public String getIsFileInPackage() {
        return isFileInPackage;
    }

    public void setIsFileInPackage(String isFileInPackage) {
        this.isFileInPackage = isFileInPackage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGlobalMimeType() {
        return globalMimeType;
    }

    public void setGlobalMimeType(String globalMimeType) {
        this.globalMimeType = globalMimeType;
    }

    public String getAttachmentCreateDate() {
        return attachmentCreateDate;
    }

    public void setAttachmentCreateDate(String attachmentCreateDate) {
        this.attachmentCreateDate = attachmentCreateDate;
    }

    public String getAttachmentModificationDate() {
        return attachmentModificationDate;
    }

    public void setAttachmentModificationDate(String attachmentModificationDate) {
        this.attachmentModificationDate = attachmentModificationDate;
    }

    @Override
    public String toString() {
        return "\tAttachement{" + "\n" +
                "\t\t\t\tfileName='" + fileName + '\'' + "\n" +
                "\t\t\t\tuniversalResourceID='" + universalResourceID + '\'' + "\n" +
                "\t\t\t\tfileID=" + fileID + "\n" +
                "\t\t\t\tversionID='" + versionID + '\'' + "\n" +
                "\t\t\t\tfileSize=" + fileSize + "\n" +
                "\t\t\t\tcheckSum='" + checkSum + '\'' + "\n" +
                "\t\t\t\tisFileInPackage='" + isFileInPackage + '\'' + "\n" +
                "\t\t\t\tdescription='" + description + '\'' + "\n" +
                "\t\t\t\tglobalMimeType='" + globalMimeType + '\'' + "\n" +
                "\t\t\t\tattachmentCreateDate='" + attachmentCreateDate + '\'' + "\n" +
                "\t\t\t\tattachmentModificationDate='" + attachmentModificationDate + '\'' + "\n" +
                "\t\t\t}";
    }
}
