package nl.response.cadacparser.dataObjecten;

import java.util.ArrayList;
import java.util.List;

public class Attachments {

    List<Attachment> attachmentsList = new ArrayList<>();

    public void addAttachment(Attachment attachment) {
        attachmentsList.add(attachment);
    }

    public List<Attachment> getAttachements() {
        return attachmentsList;
    }
}
