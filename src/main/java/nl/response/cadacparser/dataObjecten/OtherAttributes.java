package nl.response.cadacparser.dataObjecten;

import java.util.ArrayList;
import java.util.List;

public class OtherAttributes {

    private String groupLabel;
    private List<Attribute> attributes = new ArrayList<>();

    public String getGroupLabel() {
        return groupLabel;
    }

    public void setGroupLabel(String groupLabel) {
        this.groupLabel = groupLabel;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void addAttributes(Attribute attribute) {
        this.attributes.add(attribute);
    }

    public boolean containsAttributes() {
        return !attributes.isEmpty();
    }

    @Override
    public String toString() {
        return "\tOtherAttributes{" + "\n" +
                "\t\t\t\t\tgroupLabel='" + groupLabel + '\'' + "\n" +
                "\t\t\t\t\tattributes=" + attributes + "\n" +
                '}';
    }
}
