package nl.response.cadacparser.dataObjecten;

import java.util.ArrayList;
import java.util.List;

public class BillOfMaterial {

    private final List<BillOfMaterialItem> billOfMaterialsList = new ArrayList<>();

    public void addBillOfMaterialItem(BillOfMaterialItem billOfMaterialitem) {
        billOfMaterialsList.add(billOfMaterialitem);
    }

    public List<BillOfMaterialItem> getBillOfMaterials() {
        return billOfMaterialsList;
    }
}
