package nl.response.cadacparser.dataObjecten;

public class Attribute {

    private String name;
    private String value;
    private String units;
    private String dataType;
    private String dataTypeOtherDescription;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDataTypeOtherDescription() {
        return dataTypeOtherDescription;
    }

    public void setDataTypeOtherDescription(String dataTypeOtherDescription) {
        this.dataTypeOtherDescription = dataTypeOtherDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "\tAttribute{" + "\n" +
                "\t\t\t\tname='" + name + '\'' + "\n" +
                "\t\t\t\tvalue='" + value + '\'' + "\n" +
                "\t\t\t\tunits='" + units + '\'' + "\n" +
                "\t\t\t\tdataType='" + dataType + '\'' + "\n" +
                "\t\t\t\tdataTypeOtherDescription='" + dataTypeOtherDescription + '\'' + "\n" +
                "\t\t\t\tdescription='" + description + '\'' + "\n" +
                "\t\t\t}";
    }
}
