package nl.response.cadacparser.dataObjecten;

public class BillOfMaterialItem {

    private String billOfMaterialItemID;
    private String billOfMaterialItemUniqueID;
    private String billOfMaterialType;
    private String billOfMaterialTypeOtherDescription;
    private String revisionID;
    private String itemQuantity;
    private byte positionNumber;
    private byte rowOrder;
    private byte instanceCount;
    private String unitSize;
    private String detailID;
    private String billOfMaterialUnitOfMeasure;
    private String description;
    private String notes;

    public String getBillOfMaterialItemID() {
        return billOfMaterialItemID;
    }

    public void setBillOfMaterialItemID(String billOfMaterialItemID) {
        this.billOfMaterialItemID = billOfMaterialItemID;
    }

    public String getBillOfMaterialItemUniqueID() {
        return billOfMaterialItemUniqueID;
    }

    public void setBillOfMaterialItemUniqueID(String billOfMaterialItemUniqueID) {
        this.billOfMaterialItemUniqueID = billOfMaterialItemUniqueID;
    }

    public String getBillOfMaterialType() {
        return billOfMaterialType;
    }

    public void setBillOfMaterialType(String billOfMaterialType) {
        this.billOfMaterialType = billOfMaterialType;
    }

    public String getBillOfMaterialTypeOtherDescription() {
        return billOfMaterialTypeOtherDescription;
    }

    public void setBillOfMaterialTypeOtherDescription(String billOfMaterialTypeOtherDescription) {
        this.billOfMaterialTypeOtherDescription = billOfMaterialTypeOtherDescription;
    }

    public String getRevisionID() {
        if(revisionID.equals("-")) {
            return "";
        }
        return revisionID;
    }

    public void setRevisionID(String revisionID) {
        this.revisionID = revisionID;
    }

    public String getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(String itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    public byte getPositionNumber() {
        return positionNumber;
    }

    public void setPositionNumber(byte positionNumber) {
        this.positionNumber = positionNumber;
    }

    public byte getRowOrder() {
        return rowOrder;
    }

    public void setRowOrder(byte rowOrder) {
        this.rowOrder = rowOrder;
    }

    public byte getInstanceCount() {
        return instanceCount;
    }

    public void setInstanceCount(byte instanceCount) {
        this.instanceCount = instanceCount;
    }

    public String getUnitSize() {
        return unitSize;
    }

    public void setUnitSize(String unitSize) {
        this.unitSize = unitSize;
    }

    public String getDetailID() {
        return detailID;
    }

    public void setDetailID(String detailID) {
        this.detailID = detailID;
    }

    public String getBillOfMaterialUnitOfMeasure() {
        return billOfMaterialUnitOfMeasure;
    }

    public void setBillOfMaterialUnitOfMeasure(String billOfMaterialUnitOfMeasure) {
        this.billOfMaterialUnitOfMeasure = billOfMaterialUnitOfMeasure;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public String toString() {
        return "\tBillOfMaterialItem{" + "\n" +
                "\t\t\t\tbillOfMaterialItemID='" + billOfMaterialItemID + '\'' + "\n" +
                "\t\t\t\tbillOfMaterialItemUniqueID='" + billOfMaterialItemUniqueID + '\'' + "\n" +
                "\t\t\t\tbillOfMaterialType='" + billOfMaterialType + '\'' + "\n" +
                "\t\t\t\tbillOfMaterialTypeOtherDescription='" + billOfMaterialTypeOtherDescription + '\'' + "\n" +
                "\t\t\t\trevisionID=" + revisionID + "\n" +
                "\t\t\t\titemQuantity=" + itemQuantity + "\n" +
                "\t\t\t\tpositionNumber=" + positionNumber + "\n" +
                "\t\t\t\trowOrder=" + rowOrder + "\n" +
                "\t\t\t\tinstanceCount=" + instanceCount + "\n" +
                "\t\t\t\tunitSize=" + unitSize + "\n" +
                "\t\t\t\tdetailID=" + detailID + "\n" +
                "\t\t\t\tbillOfMaterialUnitOfMeasure='" + billOfMaterialUnitOfMeasure + '\'' + "\n" +
                "\t\t\t\tdescription='" + description + '\'' + "\n" +
                "\t\t\t\tnotes='" + notes + '\'' + "\n" +
                "\t\t\t}";
    }
}
