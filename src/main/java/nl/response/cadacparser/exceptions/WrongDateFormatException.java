package nl.response.cadacparser.exceptions;

/**
 * Helper class for our custom wrong date format exception.
 * Nothing fancy here. Just the basics is enough.
 */
public class WrongDateFormatException extends Exception{

    public WrongDateFormatException(String theMessage) {
        super(theMessage);
    }

    public WrongDateFormatException(String theMessage, Throwable err) {
        super(theMessage, err);
    }

}
