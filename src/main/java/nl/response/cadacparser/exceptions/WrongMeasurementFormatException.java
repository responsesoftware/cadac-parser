package nl.response.cadacparser.exceptions;

public class WrongMeasurementFormatException extends Exception {

    public WrongMeasurementFormatException(String theMessage) {
        super(theMessage);
    }

    public WrongMeasurementFormatException(String theMessage, Throwable err) {
        super(theMessage, err);
    }
}
